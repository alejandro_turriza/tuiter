from django.shortcuts import render_to_response
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib import auth
from django.contrib.auth.models import User

def home(request):
	if request.user.is_authenticated():
		return HttpResponseRedirect('/perfil/')
	return render_to_response('home.html')

def login(request):
	usr = request.POST.get('user')
	pwd = request.POST.get('password')
	if not usr or not pwd:
		return HttpResponseRedirect('/')
	user = auth.authenticate(username = usr, password = pwd)
	if user is None:
		return HttpResponseRedirect('/')
	auth.login(request, user)
	return HttpResponseRedirect('/perfil/')

def logout(request):
	auth.logout(request)
	return HttpResponseRedirect('/')

def signup(request):
	if request.method == 'POST':
		try:
			username = request.POST.get('username')
			password = request.POST.get('password')
			nombres = request.POST.get('nombre')
			apellidos = request.POST.get('apellido')
			email = request.POST.get('email')
			nuevo_usuario = User.objects.create_user(username = username, password = password)
			nuevo_usuario.first_name = nombres
			nuevo_usuario.last_name = apellidos
			nuevo_usuario.email = email
			nuevo_usuario.save()
			nuevo_usuario = auth.authenticate(username = username, password = password)
			auth.login(request, nuevo_usuario)
			return HttpResponseRedirect('/perfil/')
		except Exception as e:
			pass
	return render_to_response('signup.html')

